from yagmail.sender import SMTP

try:
    import keyring
except (ImportError, NameError, RuntimeError):
    pass


def register(username, password):
    """ Use this to add a new gmail account to your OS' keyring so it can be used in yagmail """
    keyring.set_password("yagmail", username, password)


def main():
    """ This is the function that is run from commandline with `yagmail` """
    import argparse

    parser = argparse.ArgumentParser(description="Send a (g)mail with yagmail.")
    parser.add_argument("-to", "-t", help='Send an email to address "TO"', nargs="+")
    parser.add_argument("-subject", "-s", help="Subject of email", nargs="+")
    parser.add_argument("-contents", "-c", help="Contents to send", nargs="+")
    parser.add_argument("-attachments", "-a", help="Attachments to attach", nargs="+")
    parser.add_argument("-user", "-u", help="Username")
    parser.add_argument(
        "-password", "-p", help="Preferable to use keyring rather than password here"
    )
    args = parser.parse_args()
    yag = SMTP(args.user, args.password)
    yag.send(to=args.to, subject=args.subject, contents=args.contents, attachments=args.attachments)


if __name__ == '__main__':
    sender_email = 'xxx@163.com'
    password = 'xxx'
    host = 'smtp.163.com'

    email = 'xxx@163.com'
    title = '发送结果数据utf8-123'
    attachments = ['/v-data/中文名附件测试2023.txt']
    yag = SMTP(user=sender_email, password=password, host=host)
    yag.send(to=email, subject=title, contents=title, attachments=attachments)